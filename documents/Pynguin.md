# [Pynguin](https://pynguin.readthedocs.io/en/latest/) : PYthoN General UnIt test geNerator

**AI** : No  
**Programming languages** : Python  
**Type.s of test.s** : unit

## Algorithmes

- algorithmes évolutifs > génération de tests aléatoires.
- DynaMOSA, MIO, MOSA, random, whole suite, whole suite with archive.
    - DynaMOSA : couverture plus elevée.

## Exemple

> **example.py**
```py
def triangle(x: int, y: int, z: int) -> str:  
  if x == y == z:  
           return "Equilateral triangle"  
        elif x == y or y == z or x == z:  
            return "Isosceles triangle"  
        else:  
            return "Scalene triangle"
```
> CMD
```vim
$ pynguin \
    --project-path ./docs/source/_static \
    --output-path /tmp/pynguin-results \
    --module-name example
    -v
```
> Results
```vim
[16:49:40] INFO     Start Pynguin Test Generation…                                                        generator.py:113
           INFO     Collecting static constants from module under test                                    generator.py:215
           INFO     No constants found                                                                    generator.py:218
           INFO     Setting up runtime collection of constants                                            generator.py:227
           INFO     Analyzed project to create test cluster                                                  module.py:888
           INFO     Modules:       1                                                                         module.py:889
           INFO     Functions:     1                                                                         module.py:890
           INFO     Classes:       0                                                                         module.py:891
           INFO     Using seed 1659019779632142080                                                        generator.py:201
           INFO     Using strategy: Algorithm.DYNAMOSA                                   generationalgorithmfactory.py:267
           INFO     Instantiated 11 fitness functions                                    generationalgorithmfactory.py:353
           INFO     Using CoverageArchive                                                generationalgorithmfactory.py:311
           INFO     Using selection function: Selection.TOURNAMENT_SELECTION             generationalgorithmfactory.py:286
           INFO     No stopping condition configured!                                     generationalgorithmfactory.py:92
           INFO     Using fallback timeout of 600 seconds                                 generationalgorithmfactory.py:93
           INFO     Using crossover function: SinglePointRelativeCrossOver               generationalgorithmfactory.py:299
           INFO     Using ranking function: RankBasedPreferenceSorting                   generationalgorithmfactory.py:319
           INFO     Start generating test cases                                                           generator.py:399
           INFO     Initial Population, Coverage: 1.000000                                            searchobserver.py:66
           INFO     Algorithm stopped before using all resources.                                         generator.py:402
           INFO     Stop generating test cases                                                            generator.py:407
           INFO     Start generating assertions                                                           generator.py:470
           INFO     Setup mutation controller                                                        mutationadapter.py:68
           INFO     Build AST for example                                                            mutationadapter.py:54
           INFO     Mutate module example                                                            mutationadapter.py:56
[16:49:41] INFO     Generated 14 mutants                                                             mutationadapter.py:64
           INFO     Running tests on mutant   1/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   2/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   3/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   4/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   5/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   6/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   7/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   8/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant   9/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant  10/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant  11/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant  12/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant  13/14                                               assertiongenerator.py:291
           INFO     Running tests on mutant  14/14                                               assertiongenerator.py:291
           INFO     Mutant 0 killed by Test(s): 0, 1, 2, 3, 4                                    assertiongenerator.py:369
           INFO     Mutant 1 killed by Test(s): 1, 2, 3, 4                                       assertiongenerator.py:369
           INFO     Mutant 2 killed by Test(s): 0, 2, 4                                          assertiongenerator.py:369
           INFO     Mutant 3 killed by Test(s): 0, 2, 4                                          assertiongenerator.py:369
           INFO     Mutant 4 killed by Test(s): 2, 3, 4                                          assertiongenerator.py:369
           INFO     Mutant 5 killed by Test(s): 2, 3, 4                                          assertiongenerator.py:369
           INFO     Mutant 6 killed by Test(s): 1, 2, 3, 4                                       assertiongenerator.py:369
           INFO     Mutant 7 killed by Test(s): 1, 2, 3, 4                                       assertiongenerator.py:369
           INFO     Mutant 8 killed by Test(s): 2, 3, 4                                          assertiongenerator.py:369
           INFO     Mutant 9 killed by Test(s): 0, 2, 3, 4                                       assertiongenerator.py:369
           INFO     Mutant 10 killed by Test(s): 0, 2, 4                                         assertiongenerator.py:369
           INFO     Mutant 11 killed by Test(s): 1, 2, 3, 4                                      assertiongenerator.py:369
           INFO     Mutant 12 killed by Test(s): 1, 2, 3, 4                                      assertiongenerator.py:369
           INFO     Mutant 13 killed by Test(s): 1, 2, 3, 4                                      assertiongenerator.py:369
           INFO     Number of Surviving Mutant(s): 0 (Mutants: )                                 assertiongenerator.py:381
           INFO     Written 5 test cases to /tmp/pynguin-results/test_example.py                          generator.py:569
           INFO     Writing statistics                                                                   statistics.py:354
           INFO     Stop Pynguin Test Generation…
```
> **test_example.py**
```py
# Automatically generated by Pynguin.
import example as module_0


def test_case_0():
    int_0 = 1907
    str_0 = module_0.triangle(int_0, int_0, int_0)
    assert str_0 == "Equilateral triangle"


def test_case_1():
    int_0 = 2850
    int_1 = -1201
    none_type_0 = None
    str_0 = module_0.triangle(int_0, int_1, none_type_0)
    assert str_0 == "Scalene triangle"


def test_case_2():
    int_0 = -1426
    int_1 = 2540
    str_0 = module_0.triangle(int_0, int_0, int_1)
    assert str_0 == "Isosceles triangle"
    int_2 = -463
    int_3 = -773
    int_4 = 50
    str_1 = module_0.triangle(int_2, int_3, int_4)
    assert str_1 == "Scalene triangle"
    str_2 = module_0.triangle(int_2, int_2, int_2)
    assert str_2 == "Equilateral triangle"


def test_case_3():
    int_0 = -1532
    int_1 = 1198
    str_0 = module_0.triangle(int_0, int_1, int_1)
    assert str_0 == "Isosceles triangle"
    str_1 = module_0.triangle(int_0, int_1, int_1)
    assert str_1 == "Isosceles triangle"
    none_type_0 = None
    int_2 = 1758
    int_3 = -649
    str_2 = module_0.triangle(none_type_0, int_2, int_3)
    assert str_2 == "Scalene triangle"


def test_case_4():
    int_0 = -954
    str_0 = module_0.triangle(int_0, int_0, int_0)
    assert str_0 == "Equilateral triangle"
    int_1 = -2339
    int_2 = 1529
    int_3 = -1878
    str_1 = module_0.triangle(int_1, int_2, int_3)
    assert str_1 == "Scalene triangle"
    int_4 = 672
    str_2 = module_0.triangle(int_1, int_4, int_1)
    assert str_2 == "Isosceles triangle"
```