# Papers
- [Unit Test Generation using Generative AI : A Comparative Performance Analysis of Autogeneration Tools](https://arxiv.org/abs/2312.10622)

- [An Empirical Study of Automated Unit Test Generation for Python](https://arxiv.org/abs/2111.05003)

- [Automated Unit Test Generation for Python](https://arxiv.org/abs/2007.14049)

- [Pex–White Box Test Generation for .NET](https://link.springer.com/chapter/10.1007/978-3-540-79124-9_10)

- [DART: Directed Automated Random Testing](https://web.eecs.umich.edu/~weimerw/590/reading/p213-godefroid.pdf)

- [A detailed investigation of the effectiveness of whole test suite generation](https://link.springer.com/article/10.1007/s10664-015-9424-2)

- [Many Independent Objective (MIO) Algorithm for Test Suite Generation](https://arxiv.org/abs/1901.01541)

- [Scalable Path Search for Automated Test Case Generation](https://www.researchgate.net/publication/358912479_Scalable_Path_Search_for_Automated_Test_Case_Generation#pf14)

- [CUTE: A Concolic Unit Testing Engine for C](https://dl.acm.org/doi/10.1145/1095430.1081750)

- [A Comparison of Test Generation Algorithms for Testing Application Interactions](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6004320)

- [Automatic Generation of Acceptance Test Cases from Use Case Specifications: an NLP-based Approach](https://arxiv.org/pdf/1907.08490)

# Concept

- [Random testing](https://en.wikipedia.org/wiki/Random_testing)

# Dataset
- [MBPP (Mostly Basic Python Programming)](https://huggingface.co/datasets/mbpp) : 1000 problems, solutions and tests in python. Used as benchmark.

# Tools
## AI-based :
- [CODAMOSA](https://github.com/microsoft/codamosa)
- [TICODER](https://www.microsoft.com/en-us/research/publication/interactive-code-generation-via-test-driven-user-intent-formalization/)
- [Github Copilot](https://github.com/features/copilot)
- [OpenAI Codex](https://openai.com/blog/openai-codex)
- Smartesting: [Gravity](https://www.smartesting.com/en/gravity/)

## Search-based software testing (SBST) techniques:
- [EvoSuite](https://www.evosuite.org/) : Java
- [Pynguin](https://www.pynguin.eu/) : Python

## Symbolic execution
- Pex : .NET
- [CREST](https://github.com/jburnim/crest) : C
- [UTBotCpp](https://github.com/UnitTestBot/UTBotCpp) : C/C++

# Site

- [Accelerate test-driven development with AI ](https://github.com/readme/guides/github-copilot-automattic)
Introduction to Github copilot with some indications on inside