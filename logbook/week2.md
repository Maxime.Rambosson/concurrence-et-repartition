## Date

,n

## Goal.s

Better understanding of tests concepts.

## Achievements

- Black vs White box
- Regression, Model-based

Model-based testing:
- As black box
- Online, Offline (human or computer)
- Can derive tests algorithmically: finite state machine, theorem proving, constraint logic programming, model checking, markov chain test model

Problems when generating tests (before AI):
- Low level program => Software model checking (predicate abstraction)
- Long path required (loops)
- Nondeterminism

## Problems/Obstacles encountered

<!-- TBC -->

## Next Steps

See Gantt diagram.