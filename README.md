# Concurrence et répartition : AI test code

**Course** : Concurrence et répartition. 

**Date** : Spring semester 2024.

**Authors** : 

    - Maxime Rambosson - maxime.rambosson@etu.unige.ch
    - Maxime Lefranc - maxime.lefranc@etu.unige.ch

## Description

With the rapid advancement of AI technologies, there has been a surge in the development of tools aimed at automating the testing process for software programs. This project aims to delve into the realm of AI-powered testing tools, examining their capabilities, limitations, and applicability across different programming languages and technologies.

## Objectives

**Explore the landscape of AI-powered testing tools, including but not limited to GitHub Copilot and JetBrains.**

- How AI is used in this field ?

**Investigate the types of tests these tools can generate and evaluate their adequacy in terms of code coverage.**
- Tool handling.
- What kinds of tests can be generated?
- Is the coverage adequate?

**Identify the limitations and challenges associated with these approaches.**
- Tool handling.
-  What are the limitations of these approaches?

**Assess the availability and compatibility of AI-powered testing tools across various programming languages and technologies. Pros and cons**
- Is this opportunity available for all programming languages technologies?
- Pros/Cons.

## Timeline

Based on the period from 02/26 to 05/27.

Week 1 : 02/26 - 03/04 :  
Week 2 : 03/05 - 03/11 :  
Week 3 : 03/12 - 03/18 :  
Week 4 : 03/19 - 03/25 :  
Week 5 : 03/26 - 04/01 : Easter holidays  
Week 6 : 04/02 - 04/08 :  
Week 7 : 04/09 - 04/15 :  
Week 8 : 04/16 - 04/22 :  
Week 9 : 04/23 - 04/29 :  
Week 10 : 04/30 - 05/06 :  
Week 11 : 05/07 - 05/13 :  
Week 12 : 05/14 - 05/20 :  
Week 13 : 05/21 - 05/27 :  

## References

[Github copilot](https://github.com/features/copilot)

[JetBrains](https://www.jetbrains.com/help/idea/generate-tests.html)

More : TBC